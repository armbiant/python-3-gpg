#!/bin/bash

status=0
sks -disable_mailsync -dontgossip db > /dev/null &
for v in /gnupg-* ; do
    echo "$v"
    ln -sf "${v}/usr/local/bin/gpg-agent" /usr/local/bin
    ln -sf "${v}/usr/local/bin/dirmngr" /usr/local/bin/dirmngr
    PATH="${v}/usr/local/bin/:${PATH}" gpg --version | grep "^gpg"
    PATH="${v}/usr/local/bin/:${PATH}" KEY_SERVER="hkp://127.0.0.1:11371" WITH_GPG=true python3 -m unittest test/test_gpg.py || status=1
done
exit $status
