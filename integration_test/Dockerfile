FROM biomedit/sks_keyserver

ARG GPG_V=2.0.22,2.2.8,2.2.9,2.2.10,2.2.11,2.2.12,2.2.13,2.2.14,2.2.15,2.2.16,2.2.17,2.2.20,2.2.25
# libgpg-error-dev avaialble in the distribution is incompatible with gpg < 2.2.9
ARG GPG_ERROR_V=1.36
ARG GPG_ERROR_PATH="/${GPG_ERROR_V}"

ENV LC_ALL=en_US.UTF-8 LC_LANG=en_US.UTF-8 LANGUAGE=en_US.UTF-8

RUN apt-get update \
  && apt-get install -y build-essential wget libassuan-dev libksba-dev libgcrypt-dev zlib1g-dev libpth-dev libgpg-error-dev libnpth0-dev \
  && apt-get install -y locales python3 python3-pip \
  && rm -rf /var/lib/apt/lists/* \
  && locale-gen "$LC_ALL" \
  && pkg="libgpg-error-${GPG_ERROR_V}" \
  && wget -q -O - "https://www.gnupg.org/ftp/gcrypt/libgpg-error/${pkg}.tar.bz2" | tar -xvjf - \
  && cd "$pkg" && ./configure --prefix="${GPG_ERROR_PATH}" && make && make install && cd - && rm -rf "$pkg" \
  && pip3 install dataclasses

RUN for v in $(echo "$GPG_V" | tr , " ") ; do \
    pkg="gnupg-${v}" ; \
    cd /tmp/ \
    && wget -q -O - "https://gnupg.org/ftp/gcrypt/gnupg/${pkg}.tar.bz2" | tar -xvjf - \
    && dpkg --compare-versions "${v}" "lt" "2.2.9" && LIBGPG_ERROR_PREFIX="--with-libgpg-error-prefix=${GPG_ERROR_PATH}" || LIBGPG_ERROR_PREFIX="" \
    && cd "$pkg" && ./configure "${LIBGPG_ERROR_PREFIX}" && make && mkdir "/$pkg" && DESTDIR="/$pkg" make install || exit 1 ; \
    cd - && rm -rf "$pkg" ; \
    done

RUN ln -sf /gnupg-2.0.22/usr/local/bin/gpg2 /gnupg-2.0.22/usr/local/bin/gpg
