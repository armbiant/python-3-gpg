# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.12.3](https://gitlab.com/biomedit/gpg-lite/compare/0.12.2...0.12.3) (2022-08-11)


### Bug Fixes

* add proxy support to VKS functions ([0a79138](https://gitlab.com/biomedit/gpg-lite/commit/0a79138f683eac192acfeb392916c6f09eecb1bf)), closes [#39](https://gitlab.com/biomedit/gpg-lite/issues/39)
* **keyserver:** unquote email in KeyserverKeyNotFoundError ([2daf91d](https://gitlab.com/biomedit/gpg-lite/commit/2daf91df17841a92abc48955759960fc46878730)), closes [#38](https://gitlab.com/biomedit/gpg-lite/issues/38)

### [0.12.2](https://gitlab.com/biomedit/gpg-lite/compare/0.12.1...0.12.2) (2022-07-22)


### Features

* **keyserver:** add support for Verifying Keyserver (VKS) Interface ([6ecd153](https://gitlab.com/biomedit/gpg-lite/commit/6ecd153bbf8a3d1e44752270df11891cb5a8010a)), closes [#36](https://gitlab.com/biomedit/gpg-lite/issues/36)

### [0.12.1](https://gitlab.com/biomedit/gpg-lite/compare/0.12.0...0.12.1) (2022-03-30)


### Features

* **GPGStore:** add export secret key method ([3de1c5d](https://gitlab.com/biomedit/gpg-lite/commit/3de1c5df2f6133cd7cb0bd900e3ab31912ca27e3)), closes [#32](https://gitlab.com/biomedit/gpg-lite/issues/32)

## [0.12.0](https://gitlab.com/biomedit/gpg-lite/compare/0.11.0...0.12.0) (2021-11-11)


### ⚠ BREAKING CHANGES

* **typing:**   * passphrase argument of GPGStore.revoke_key is now mandatory
  * Unified types accepted by all src and output arguments of GPGStore methods

### Features

* **typing:** Comply with mypy --strict ([383ac98](https://gitlab.com/biomedit/gpg-lite/commit/383ac9824ab2ac9cc6ee196cbc34827f4534968f))

## [0.11.0](https://gitlab.com/biomedit/gpg-lite/compare/0.10.2...0.11.0) (2021-10-13)


### ⚠ BREAKING CHANGES

* KeyCapability is now an enum, "unknown" moved to SpecialKeyCapability

### Features

* add support for "~" shortcut in gnupg_home_dir argument of GPGStore ([b6fdc9f](https://gitlab.com/biomedit/gpg-lite/commit/b6fdc9fa8af583530a95356d4887a5ac6a53725d)), closes [#30](https://gitlab.com/biomedit/gpg-lite/issues/30)
* extract key fingerprint instead of key id from signatures ([8b7f834](https://gitlab.com/biomedit/gpg-lite/commit/8b7f834dda31688d5943d022767118dffe6df792)), closes [#29](https://gitlab.com/biomedit/gpg-lite/issues/29)
* gen_key: Support for ECC keys and unprotected keys ([e637134](https://gitlab.com/biomedit/gpg-lite/commit/e637134289b5b4234df3819439de4e063b8d1858))


### Bug Fixes

* improve error msg when GnuPG binary is not found ([047d229](https://gitlab.com/biomedit/gpg-lite/commit/047d22965d87768fa68f7b06f2298f43f8c1c9af))
* **keyserver:** fix typo in the error message ([c965c2e](https://gitlab.com/biomedit/gpg-lite/commit/c965c2edf2c93ad0588fbb735f1b55cf281486e5))


* Simplify key capabilities ([4a3496c](https://gitlab.com/biomedit/gpg-lite/commit/4a3496c4506d55e45849b952cb2d71236a99e99f))

### [0.10.2](https://gitlab.com/biomedit/gpg-lite/compare/0.10.1...0.10.2) (2021-08-13)


### Bug Fixes

* **decrypt:** fail decryption on invalid MDC consistently across GPG versions ([a8595f3](https://gitlab.com/biomedit/gpg-lite/commit/a8595f3493f67bcc01f4183fbb5cd0b920fde61b)), closes [#27](https://gitlab.com/biomedit/gpg-lite/issues/27)

### [0.10.1](https://gitlab.com/biomedit/gpg-lite/compare/0.10.0...0.10.1) (2021-07-28)


### Features

* add keyserver error handling ([d321209](https://gitlab.com/biomedit/gpg-lite/commit/d32120963a17e6ce5ce7cc38e140ca7030a33130)), closes [#25](https://gitlab.com/biomedit/gpg-lite/issues/25)

## [0.10.0](https://gitlab.com/biomedit/gpg-lite/compare/0.9.0...0.10.0) (2021-06-24)


### ⚠ BREAKING CHANGES

*  * add required arguments "issuer_fingerprint" and
   "validity" to the class Signature.
* Some symbols are no longer visible from the toplevel module and now have to be imported via a submodule

### Bug Fixes

* add fingerprint and validity to signatures objects ([c38d902](https://gitlab.com/biomedit/gpg-lite/commit/c38d9025600395f7d77a77683d5b01d5d04b2871)), closes [#23](https://gitlab.com/biomedit/gpg-lite/issues/23)
* update valid_signatures method to exclude non-verified signatures ([982cdfd](https://gitlab.com/biomedit/gpg-lite/commit/982cdfd9ccd81877c00b85782a374587ebb9970c))


* modularize package ([2c76406](https://gitlab.com/biomedit/gpg-lite/commit/2c76406df482c700b63b11e5eef7f70c6567b523))

## [0.9.0](https://gitlab.com/biomedit/gpg-lite/compare/0.8.0...0.9.0) (2021-06-11)


### ⚠ BREAKING CHANGES

* the following argument names were changed:
* GPGStore(): "config_dir" changed to "gnupg_home_dir".
* list_pub_keys(), list_sec_keys(), _list_keys(): "keys" changed
    to "search_terms".
* search_keys(): "name" changed to "search_term".
* detach_sig(): "user" changed to "signee".
* delete_keys() renamed to delete_pub_keys().
* revoke() renamed to revoke_key().
* search_keys() renamed to search_keyserver().
* verify() renamed to verify_detached_sig().

### Bug Fixes

* remove check for matching creation and expiration date ([665f2e5](https://gitlab.com/biomedit/gpg-lite/commit/665f2e5dd6015e2c30f2b2ad424c03dda15d3617)), closes [#21](https://gitlab.com/biomedit/gpg-lite/issues/21)
* set encoding to utf8 explicitly in setup.py. Add AUTHORS.md ([9955dc5](https://gitlab.com/biomedit/gpg-lite/commit/9955dc529b06f36b5d46f994cb7ac5c0f7c6628f))


* rename arguments in some of the gpg-lite functions. Add documentation ([ac17221](https://gitlab.com/biomedit/gpg-lite/commit/ac172215c5afbe319cec981b10c9bb1ede9679c9)), closes [#22](https://gitlab.com/biomedit/gpg-lite/issues/22)

## [0.8.0](https://gitlab.com/biomedit/gpg-lite/compare/0.7.2...0.8.0) (2021-04-13)


### ⚠ BREAKING CHANGES

* GPGStore.export() now directly returns a bytes object instead of a file object

* GPGStore.export(): Change return type to bytes ([49945ea](https://gitlab.com/biomedit/gpg-lite/commit/49945ead0ab7a784dcd99874c0bcdae4ec386e0d))

### [0.7.2](https://gitlab.com/biomedit/gpg-lite/compare/0.7.1...0.7.2) (2020-11-23)


### Features

* **encrypt:** add compression algorithm and level options ([393932c](https://gitlab.com/biomedit/gpg-lite/commit/393932c8e2911d2c3196a8c17a52621ca7ebaec1)), closes [#16](https://gitlab.com/biomedit/gpg-lite/issues/16)


### Bug Fixes

* **cmd:** re-raise exceptions from child threads ([f894c8b](https://gitlab.com/biomedit/gpg-lite/commit/f894c8bea8f035b1986d23de622d35155460d213)), closes [#15](https://gitlab.com/biomedit/gpg-lite/issues/15)
* **Uid:** allow missing name or email in key UID ([0a037d7](https://gitlab.com/biomedit/gpg-lite/commit/0a037d78fe8e6a9f2ef69b571b708f5350718aa4)), closes [#17](https://gitlab.com/biomedit/gpg-lite/issues/17)

### [0.7.1](https://gitlab.com/biomedit/gpg-lite/compare/0.7.0...0.7.1) (2020-10-26)

## [0.7.0](https://gitlab.com/biomedit/gpg-lite/compare/0.6.13...0.7.0) (2020-10-23)


### ⚠ BREAKING CHANGES

* interfaces to encrypt and decrypt changed

    decrypt is a regular function (instead of a context manager)
    returning signee's key fingerprints
    encrypt returns None

### Features

* encrypt and decrypt accept callables for input and output ([87fc5f8](https://gitlab.com/biomedit/gpg-lite/commit/87fc5f8a20423803ea7bd378b6cb19addf95d031))


### Bug Fixes

* **deserialize:** use correct import for collections.abc ([176807d](https://gitlab.com/biomedit/gpg-lite/commit/176807d044140c63f9e1f8295a528457151df234))

### [0.6.13](https://gitlab.com/biomedit/gpg-lite/compare/0.6.12...0.6.13) (2020-08-07)


### Bug Fixes

* **packaging:** Add py.typed ([756cb96](https://gitlab.com/biomedit/gpg-lite/commit/756cb964878c114cf39a1cddefdcdc0fba2f7af5))

### [0.6.12](https://gitlab.com/biomedit/gpg-lite/compare/0.6.11...0.6.12) (2020-08-07)


### Features

* **extract_key_id:** Improve verbosity for exceptions ([a4db24e](https://gitlab.com/biomedit/gpg-lite/commit/a4db24eb85c013004cd4c625eff143696b8046a1))


### Bug Fixes

* **GPGStore.verify, GPGStore.decrypt:** preserve stderr for error handling ([3b328be](https://gitlab.com/biomedit/gpg-lite/commit/3b328be4409e8e7c7a75a082f861a9db1a1fca53))

### [0.6.11](https://gitlab.com/biomedit/gpg-lite/compare/0.6.10...0.6.11) (2020-08-04)


### Bug Fixes

* **extract_key_id_from_sig:** Support windows formatted ascii armored signatures ([80cee6a](https://gitlab.com/biomedit/gpg-lite/commit/80cee6a9e36795bd134b7aeb1da7a0721fdbcb59))

### [0.6.10](https://gitlab.com/biomedit/gpg-lite/compare/v0.6.9...v0.6.10) (2020-07-22)


### Features

* **extract_key_id_from_sig:** Support for ascii armored signatures ([03387a9](https://gitlab.com/biomedit/gpg-lite/commit/03387a9a01321b889d0b9244252014b963114307))


### Bug Fixes

* **typehints:** Add Optional t keys argument of list_pub_keys, list_sec_keys ([762322e](https://gitlab.com/biomedit/gpg-lite/commit/762322ec9bd52f846faed130316dcdb371dbbdaf))

### [0.6.9](https://gitlab.com/biomedit/gpg-lite/compare/0.6.8...0.6.9) (2020-07-09)


### Features

* **extract_key_id:** Support detached signatures ([d35e589](https://gitlab.com/biomedit/gpg-lite/commit/d35e5895030436c5ae82fb7bc5438934f36dfd2b))


### Bug Fixes

* **list_*_keys:** Make keys argument optional, return [] if keys is empty. Return all keys if keys is None ([c01e75f](https://gitlab.com/biomedit/gpg-lite/commit/c01e75f5f34253794d62f2c65187fb1dab29eea5))

### 0.6.8 (2020-07-06)


### Bug Fixes

* replace non UTF-8 characters in gpg output to avoid python decode error ([61f055e](https://gitlab.com/biomedit/gpg-lite/commit/61f055eeedd94ffb5e8db79b0ecdff239148f7f4))
* signature verification on Windows ([6e33f2f](https://gitlab.com/biomedit/gpg-lite/commit/6e33f2f6d4ba73f37348ff22ca90b7c2317b2a51))
* work around french gpg returning non utf-8 symbols ([1bd05b8](https://gitlab.com/biomedit/gpg-lite/commit/1bd05b8aab7e4b772b9f12253af771f53fee3c34))
