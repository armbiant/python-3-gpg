# Contributing to gpg-lite

## Commit Message Guidelines
This project follows the [Angular commit message guidelines](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-guidelines)
for its commit messages.


### Template
Fields shown in `[square brackets]` are optional.
```
type[(scope)]: subject line - max 100 characters

[body] - extended description of commit that can stretch over
multiple lines. Max 100 character per line.

[footer] - links to issues with (Closes #, Fixes #, Relates #) and BREAKING CHANGE:
```


### Type and keywords summary
##### Type
The following types are allowed. Only commits with types shown in **bold** are automatically
added to the changelog (and those containing the `BREAKING CHANGE: ` keyword):
* **feat**: new feature
* **fix**: bug fix
* build: changes that affect the build system or external dependencies.
* ci: changes to CI configuration files and scripts.
* docs: documentation only changes
* perf: code change that improves performance.
* refactor: code change that neither fixes a bug nor adds a feature
* style: change in code formatting only (no effect on functionality).
* test: change in unittest files only.

##### Scope
* name of the file/functionality/workflow affected by the commit.

##### Subject line
* one line description of commit with max 100 characters.
* use the imperative form, e.g. "add new feature" instead of "adds new feature" or
  "added a new feature".
* no "." at the end of subject line.

##### body
* Extended description of commit that can stretch over multiple lines.
* Max 100 characters per line.
* Explain things such as what problem the commit addresses, background info on why the change
  was made, alternative implementations that were tested but didn't work out.

##### footer
* Reference to git issue with `Closes/Close`, `Fixes/Fix`, `Related`.
* Location for `BREAKING CHANGE: ` keyword. Add this keyword followed by a description of what
  the commit breaks, why it was necessary, and how users should port their code to adapt to the
  new version. Any commit containing the pattern `BREAKING CHANGE: ` will be added to the
  changelog, regardless of its type.


### Commit messages and auto-versioning
The following rules are applied by the auto-versioning system to modify the version number when a
new commit is pushed to the `master` branch:
* Keyword `BREAKING CHANGE: `: increases the new major digit, e.g. 1.1.1 -> 2.0.0
* Type `feat`: increases the minor version digit, e.g. 1.1.1 -> 1.2.0
* Type `fix`: increases the patch digit, e.g. 1.1.1 -> 1.1.2

**Note:** an exception to the behavior of `BREAKING CHANGE: ` is for pre-release versions (i.e.
0.x.x). In that case, a `BREAKING CHANGE: ` keyword increases the minor digit rather than the
major digit. For more details, see the
[conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) and the
[semantic versioning](https://semver.org/spec/v2.0.0.html) specifications.


### Example
```
feat(encrypt): add compression algorithm and level options

Add 'compress_algo' and 'compress_level' arguments to the encrypt() function.
These options allow to specify the encryption algorithm to be used by gpg
as well as the compression level (in the range 0 - 9). These two arguments
are optional.

Closes #16, Related #21
```
