
# Maintainers of gpg-lite

* Gerhard Bräunlich
* Jaroslaw Surkont
* François Martin
* Christian Ribeaud
* Robin Engler
* Simone Guzzi

# Patches and Suggestions

* Tutor Exilius
* Dorian Jaminais-Grellier
